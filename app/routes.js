var path = require('path');
var url = require('url');
var User = require('./models/user');
var githubScrapperService = require('./services/github-scrapper-service');

module.exports = function(app) {
	//==================================================
	// Routes
	//==================================================

	// server routes ==================================================
	// handle things like api calls

	// sample api route
	app.get('/api/users', function(req,res){
		// user mongoose to get all users in the database
		/*User.find(function(err, nerds){
			// if there is an error retrieving, send the error.
			// nothing after res.send(err) will execute
			if(err)
				res.send(err);

			// return all users in json format
			res.json(users);
		});*/
		//console.log(2);
		var query = url.parse(req.url, true).query;
		githubScrapperService.getFilteredUsersByRepository(query.repoIndex).then(function(data) {
			actionJsonResponse(res, data);
		});
	});

	// route to handle creating goes here (app.post)
	// route to handle delete goes here (app.delete)

	// frontend routes ==================================================
	// route to handle all angular requests
	app.get('', function(req, res){
		// FIXME: There should be a way in which we can directly specify the
		// relative file path and it automatically looks inside 'public/views/'
		res.sendFile('index-new.html',
			{root: path.join(__dirname, '../public/views')});
	});


	//==================================================
	// Private Methods
	//==================================================

	function actionJsonResponse(res, data) {
		res.setHeader('Content-Type', 'application/json');;
		res.send(JSON.stringify(data, null, 2));
	}
}
