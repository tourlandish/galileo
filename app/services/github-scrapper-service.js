var githubService = require('./github-service');

module.exports = {
	getFilteredUsersByRepository: getFilteredUsersByRepository
}

//==================================================
// Public Methods
//==================================================

function getFilteredUsersByRepository(index) {
	return githubService.getTopRepository(index).then(function(repo) {
		return githubService.getAllContributors(repo).then(function(contributors) {
			// Add contributors length to repo.
			repo.contributors = contributors.length;

			return githubService.getAllUsersUsingLogin(contributors.map(function(val){
				return val.login;
			})).then(function(users){
				// Filter out people only in India
				return {
					repo: repo,
					users: users.reduce(function(reduced, val, index){
						if(val.location) {
							var keyword = "India"
							var index = val.location.indexOf(keyword);
							if(index != -1
									&& (index == 0 || val.location[index-1] == ' ')
									&& ((index + keyword.length) == val.location.length
										|| val.location[index + keyword.length] == ' ')){
								val.contributions = contributors[index].contributions;
								reduced.push(val);
							}
						}
						return reduced;
					}, [])
				}
			});
		});
	});
}
