var request = require("request");
var Promise = require("bluebird");

Promise.onPossiblyUnhandledRejection(function(error){
	throw error;
});

module.exports = {
	getTopRepository: getTopRepository,
	getAllContributors: getAllContributors,
	getAllUsersUsingLogin: getAllUsersUsingLogin
}

//==================================================
// Private Variables
//==================================================

// Access token for test-headout account
var access_token = '434768738eec62577de877bddc88df4694d1f525';
var per_page = 100;	// 100 is max

//==================================================
// Private Methods
//==================================================

function accessTokenify(url) {
	// TODO: Are there StringBuilder alternatives over here?
	if(url.indexOf('?') == -1)
	url += '?';
	if(url.indexOf('?') != url.length-1)
	url += '&';
	return url += 'access_token=' + access_token;
}

function createGithubRequestOptions(url) {
	return {
		url: accessTokenify(url),
		headers: {
			'User-Agent': 'headout-galileo/1.0.0'
		}
	}
}

function makeRequest(options, returnJsonParsedBody) {
	return new Promise(function(fulfill, reject) {
		request(options, function(error, response, body) {
			if(!error && response.statusCode == 200) {
				if(returnJsonParsedBody)
					fulfill(JSON.parse(body));
				else
					fulfill(response);
			} else if (error) {
				reject(error);
			} else {
				reject("response.statusCode=" + response.statusCode
					+ "\n\n" + body);
			}
		});
	});
}

/**
 * Converts the link header to an object specifying a map of rel & it's corresponding link
 * Eg:
 * link: <https://api.github.com/repositories/460078/contributors?page=15>; rel="next",
 * <https://api.github.com/repositories/460078/contributors?page=15>; rel="last",
 * <https://api.github.com/repositories/460078/contributors?page=1>; rel="first",
 * <https://api.github.com/repositories/460078/contributors?page=13>; rel="prev"
 *
 * return: {
 * 		next: "https://api.github.com/repositories/460078/contributors?page=15",
 *		last: "https://api.github.com/repositories/460078/contributors?page=15",
 * 		first: "https://api.github.com/repositories/460078/contributors?page=1",
 * 		prev: "https://api.github.com/repositories/460078/contributors?page=13"
 * }
 */
function getLinkHeaderSplit(link) {
	if(!link)
		return null;

	var linkSplit = link.split(',');
	var links = {};
	for(var i in linkSplit) {
		var linkElem = linkSplit[i].split(';');
		links[linkElem[1].split('=')[1].trim().slice(1, -1)] = linkElem[0].trim().slice(1, -1);
	}

	return links;
}

function getUrlParamVal(url, param) {
	var urlSplit = url.split('?');
	if(urlSplit[1]) {
		return urlSplit[1].split('&').reduce(function(last, val) {
			// If we already got the page value then just return it.
			if(last)
				return last;

			// See if we have the page value and return it.
			var valSplit = val.split('=');
			if(valSplit[0] == param) {
				return valSplit[1];
			}
		}, undefined);
	}
}


//==================================================
// Public Methods
//==================================================

function getTopRepository(index) {
	console.log("Getting top repository for index: " + index);

	// Define the languages for which the repositories will be searched.
	var languages = ['javascript', 'java', 'html', 'css', 'swift', 'objectivec', 'typescript'];

	// Create our URL.
	var url = 'https://api.github.com/search/repositories?sort=stars&q=';
	for(var i in languages) {
		if(i>0)
			url += '+';
		url += 'language:' + languages[i];
	}
	url += '&per_page=' + per_page;
	url += '&page=' + parseInt((index / per_page) + 1);

	return makeRequest(createGithubRequestOptions(url)).then(function(response) {
		var data = JSON.parse(response.body);
		var actualIndex = index % per_page;

		// Return the specified index repository
		// Note: The index may not exist
		if(data.items && data.items.length > actualIndex) {
			return data.items[actualIndex];
		} else {
			return null;
		}
	});
}

function getAllContributors(repo) {
	console.log("Getting all contributors for repo. repo.owner.login: "
		+ repo.owner.login + "; repo.name: " + repo.name);

	var url = "https://api.github.com/repos/" + repo.owner.login + '/' + repo.name + '/contributors?per_page=' + per_page;

	// Send the first request
	return makeRequest(createGithubRequestOptions(url + "&page=1")).then(function (response){
		var users = JSON.parse(response.body);
		var links = getLinkHeaderSplit(response.headers.link);

		console.log("#################### Links: \n" + JSON.stringify(links));

		// If we have more pages, fetch all the pages concurrently, combine them and return the result.
		if(links && links.last) {
			var lastPage = Number(getUrlParamVal(links.last, "page"));
			var arrPagePromises = [];

			for(var i=2; i<=lastPage; i++) {
				arrPagePromises.push(makeRequest(createGithubRequestOptions(url + "&page=" + i)));
			}

			return Promise.all(arrPagePromises).then(function(arrUsers) {
				for(var i in arrUsers){
					var newUsers = JSON.parse(arrUsers[i].body);
					console.log(newUsers.length);
					for(var j in newUsers)
						users.push(newUsers[j]);
				}
				console.log(users.length);
				return users;
			});
		} else {
			return users;
		}
	});
}

function getAllUsersUsingLogin(logins) {
	console.log("Getting all users for the logins:\n", JSON.stringify(logins)
		, "\n# users: " + logins.length);
	var _url = "https://api.github.com/users/";

	return Promise.all(logins.map(function(val){
		return makeRequest(createGithubRequestOptions(_url + val)).then(function(response){
			return JSON.parse(response.body);
		});
	}));
}
