// grab the mongoose module
var mongoose = require('mongoose');

// define our use model
// module.exports allows us to pass this to other files when it is called.
module.exports = mongoose.model('User', {
	username: {type: String},
	name: {type: String},
	email: {type: String},
	website: {type: String}
})
