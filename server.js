const express			= require('express');
const fs 				= require('fs');
const bodyParser		= require('body-parser');
const https				= require('https');

const httpUtil			= require('./app/utils/http-utils');
const envConfig			= require('./config/env');

const app				= express();

//==================================================
// Configuration
//==================================================

// config files
// var db = require('./config/db');
// var httpsPort=3443;

// set our port
// var port = process.env.PORT || 3000;

// connect to our mongoDB database
// (uncomment after you enter in your own credentials in config/db.js)
// mongoose.connect(db.url)

// get all data/stuff of the body (POST) parameters

// parse application/json
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({type: 'application/vnd.api+json'}))

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
// app.use(methodOverride('X-HTTP-Method-Override'));

// set the static files location. /public/img will be /img for users
app.use(express.static(__dirname + '/public'));


//==================================================
// Filters
//==================================================

// Redirect all HTTP traffic to HTTPS
// FIXME: Somehow it seems the server is not even listening to HTTP. Not sure what's happening.
app.set('port_https', envConfig.httpsPort);
app.all('*', function(req, res, next) {console.log(1);
	if(req.secure) {
		return next();
	}
	res.redirect('https://' + req.hostname + ':' + app.get('port_https') + req.url);
});

// Configure Basic HTTP Auth
app.all('*', httpUtil.basicAuth('goku', 'kakarot'));


//==================================================
// Routes
//==================================================

require('./app/routes')(app); // configure our routes


//==================================================
// Server Start
//==================================================

// Setup HTTPS
var options = {
	key: fs.readFileSync('./config/ssl/private.key'),
	cert: fs.readFileSync('./config/ssl/certificate.pem')
}
// startup our app at http://localhost:3000
// app.listen(port);
var httpServer = https.createServer(options, app)
						.listen(envConfig.httpsPort);

// shoutout to the user
console.log('Server started on port ' + envConfig.httpsPort);

// expose app
exports = module.exports = app;
